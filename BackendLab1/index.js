const express = require('express')
const app = express()
const port = 4000

app.get('/', (req, res) => {
  res.send('Hello World!')
})


app.post('/bmi',(req, res) => {
    let weight = parseFloat(req.query.weight)
    let height = parseFloat(req.query.height)
    var result = {}

    if( !isNaN(weight)&& !isNaN(height) ){
        let bmi = weight / (height*height)
        result = {
            "status" : 200,
            "bmi" : bmi
        }
    }else {
        result ={
            "status" : 400,
            "massage" : "Weight  or Height is not a number"
        }
    }
    
    res.send(JSON.stringify(result))

})

app.get('/triangle',(req, res) => {
    let base = parseFloat(req.query.base)
    let height = parseFloat(req.query.height)

    if( !isNaN(base)&& !isNaN(height) ){
        let triangle = 0.5*base*height
        result = {
            
            "triangle" : triangle
        }
    }else {
        result ={
            
            "massage" : "base  or Height is not a number"
        }
    }
    
    res.send(JSON.stringify(result))

})


app.post('/score',(req, res) => {
    
    let name = String(req.query.name)
    let score = parseFloat(req.query.score)
    
        if((score >= 81)&&(score <=100)){
            result={
               "Name" : name,
               "grade" : "A" 
            }
        }else if(score >= 71){
            result={
                "Name" : name,
                "grade" : "B" 
            }
        }else if(score >= 61){
            result={
                "Name" : name,
                "grade" : "C" 
            }
        }else {
            result={
                "Name" : name,
                "grade" : "D" 
            }
        }
    
    res.send(JSON.stringify(result))

})


app.get('/hello', (req, res) => {
    res.send('Sawasdee '+req.query.name)
  })

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})