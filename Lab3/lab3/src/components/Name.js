import './Name.css';

function Name(){

    return(
        <div id='bg3'>
            <h1 id='l4'>ชื่อหนัง : ไททานิก</h1>
            
            <center>
            <iframe width="560" height="315" src="https://www.youtube.com/embed/j8ICzHClHK4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </center>
            <p>
            ประเภท : โรแมนติก / ดราม่า
            </p>
            <p>
            ผู้กำกับ : เจมส์ คาเมรอน
            </p>
            <p>
            นักแสดง : ลีโอนาร์โด ดิคาปริโอ, เคท วินสเลต, บิลลี่ เซน, บิลล์ แพ็กซ์ตัน
            </p>

             <hr/>
        </div>
    );
}

export default Name;