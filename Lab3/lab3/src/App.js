import logo from './logo.svg';
import './App.css';
import Header from './components/Header';
import Name from './components/Name';
import Body from './components/Body';
import Footer from './components/Footer';


function App() {
  return (
    <div className="App">
      <Header />
      <Name />
      <Body />
      <Footer />
    </div>
  );
}

export default App;
