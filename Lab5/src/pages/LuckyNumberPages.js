
import { useEffect, useState } from "react";
import LuckyNumber from "../components/LuckyNumber";

function LuckyNumberPage (){

    const[numlucky,setNumLucky] = useState("");
    const[luckyresult,setLuckyResult] = useState("");
    const[randomnumber,setRandomnumber]= useState(0);

    useEffect(() => {setRandomnumber(Math.round((Math.random()*99)+0))
    });

    function Calluck(){
        let lucky = parseInt(numlucky);
        let rd = parseInt(randomnumber);

        if (lucky == "69" || lucky == "rd" ){
            setLuckyResult("ถูกแล้วจ้าา")
        }
        else {
            setLuckyResult("ผิดนะจ๊ะ")
        }
    }



    return(
        <div align = "left">
            <div align = "center">
            ยินดีต้อนรับสู่การเสี่ยงทาย 
                <hr/>

                กรุณาทายตัวเลขที่ต้องการ ระหว่าง 0-99: <br/><input type = "text"
                              value={numlucky}
                              onChange={ (e) => { setNumLucky(e.target.value) } } /> <br />
            <button onClick={ ()=>{ Calluck () } }> ทาย </button>

        

        { numlucky != 0 &&

            <div>
                <br/>นี่คือผลการทาย
            <LuckyNumber
            numresult = {luckyresult}
            />
            </div>

         }
            </div>
        </div>

    );
}
export default LuckyNumberPage;