import logo from './logo.svg';
import './App.css';

import AboutUsPages from './pages/AboutUsPages';
import BMICalPages from './pages/BMICalPages';
import Header from './components/Header';

import {Routes,Route} from "react-router-dom";
import LuckyNumberPages from './pages/LuckyNumberPages';

function App() {
  return (
    <div className="App">
      <Header />
        <Routes>

          <Route path="about" element={
                  <AboutUsPages/>
                  }/>
          <Route path="/" element={
            <BMICalPages/>
            }/>
          <Route path="lucky" element={
            <LuckyNumberPages/>
            }/>
        
        </Routes>
    </div>
  );
}

export default App;
